# Based on the file created for Arch Linux by:
# Tobias Powalowski <tpowa@archlinux.org>
# Thomas Baechler <thomas@archlinux.org>

# Maintainer: Philip Müller (x86_64) <philm@manjaro.org>
# Maintainer: Jonathon Fernyhough (i686) <jonathon@manjaro.org>

# put together with file form https://github.com/raymond-w-ko/pkgbuilds/blob/master/linux-ck/PKGBUILD
# Maintainer Jared Ward <jroddius@gmail.com>

pkgbase=desktop-hardened-lts-419
pkgname=("$pkgbase" "$pkgbase-headers")
_kernelname="-desktop-hardened-lts"
_basekernel=4.19
_basever=419
_aufs=20190211
_bfq=v9
_bfqdate=20190204
# This sublevel may not match the package. The sub may be changed during the
# running of this PKGBUILD
_sub=32
_commit=
pkgver=${_basekernel}.${_sub}
pkgrel=1
arch=('i686' 'x86_64')
url="http://www.kernel.org/"
license=('GPL2')
makedepends=('xmlto' 'docbook-xsl' 'kmod' 'inetutils' 'bc' 'elfutils' 'git')
options=('!strip')
source=("https://github.com/anthraxx/linux-hardened/archive/4.19-lts.zip"
        # the main kernel config files
        'config.x86_64.generic' 'config.x86_64.amd' 'config.x86_64.intel' 'config.x86_64.native' 'config' 'config.aufs'
        "${pkgbase}.preset" # standard config files for mkinitcpio ramdisk
        '60-linux.hook'     # pacman hook for depmod
        '90-linux.hook'     # pacman hook for initramfs regeneration
        "aufs4.19.17+-${_aufs}.patch.bz2"
        'aufs4-base.patch'
        'aufs4-kbuild.patch'
        'aufs4-loopback.patch'
        'aufs4-mmap.patch'
        'aufs4-standalone.patch'
        'tmpfs-idr.patch'
        'vfs-ino.patch'
        #"0001-BFQ-${_bfq}-${_bfqdate}.patch::https://github.com/Algodev-github/bfq-mq/compare/0adb328...698937e.patch"
        0001-BFQ-${_bfq}-${_bfqdate}.patch::https://raw.githubusercontent.com/sirlucjan/kernel-patches/master/4.19/bfq-sq-mq/4.19-bfq-sq-mq-v9r1-2K190204-rc1.patch
        # ARCH Patches
        '0001-add-sysctl-to-disallow-unprivileged-CLONE_NEWUSER-by.patch'
        # MANJARO Patches
        '0001-i2c-hid-override-HID-descriptors-for-certain-devices.patch'
        '0002-i2c-hid-properly-terminate-i2c_hid_dmi_desc_override_table_array.patch'
        # Bootsplash
        '0001-bootsplash.patch'
        '0002-bootsplash.patch'
        '0003-bootsplash.patch'
        '0004-bootsplash.patch'
        '0005-bootsplash.patch'
        '0006-bootsplash.patch'
        '0007-bootsplash.patch'
        '0008-bootsplash.patch'
        '0009-bootsplash.patch'
        '0010-bootsplash.patch'
        '0011-bootsplash.patch'
        '0012-bootsplash.patch'

        #ck1 patchset
        #'http://ck.kolivas.org/patches/4.0/4.19/4.19-ck1/patch-4.19-ck1.xz'
        ck1.new.patch

        # skip_list for ck1 patch
	      #'skip_list.h'

        # cpu optimiztion patch from graysky
        'https://raw.githubusercontent.com/graysky2/kernel_gcc_patch/master/enable_additional_cpu_optimizations_for_gcc_v8.1%2B_kernel_v4.13%2B.patch'

	      #exfat-nofuse module
	      'https://github.com/barrybingo/exfat-nofuse/archive/master.zip'
	      'exfat.patch'
        )

sha256sums=('d20a054596ba8d7d1f441b8e46f9d1a1806e0715b391ee90e2c416bbfd88cb74'
            '7a05187fa85e0e608373f88f328a343456febb716827f2cb4f7913d08b457b94'
            'f2724540d929dece5059cb0e1f2631765b71ec1f45a9a3ec1f1d8eb3073fa87e'
            'af19e53e9c4d227db9abb1dab75d44adc1c7f1d951e3acab4bceddd8b4c811c4'
            '2844953d69ecb24311b052b54ffc6b109273f122ab723fef595c96213bf83d43'
            'cf9f1917c4570d52b0b88c41c26da42fe65ffca3cb7c562413f2d85c4fb84853'
            'b44d81446d8b53d5637287c30ae3eb64cae0078c3fbc45fcf1081dd6699818b5'
            '58e92260065b104d03a56e87e1b9ada5cd2e8ccd3259813bc41bc79fc81a7039'
            '11e1c8fb9c8d99a3d1bc13e346027858f0f818738f747c3d3cda034cf6404c19'
            'ae2e95db94ef7176207c690224169594d49445e04249d2499e9d2fbc117a0b21'
            '911512e04594c24ccf7da8a2fb3ba93bcbcf89d354c702ac6a308e6a628e1147'
            '7e5db8cfc37ea6f27107909d07cb923bf23465aadf15e9f67a826638e988611a'
            'bb7dab3f010db7e0f3e67edd8526b2735f19086413a36ab77ba5c4af02013c9d'
            '59708da839c4ce99ed05c3f7747e6c6551cd5e84d0c2a75b856f419856f0ff3b'
            '0c4c6d54f4486ade957ee517ea0122941bc25a4ab445fccdb57146244257e5b7'
            '0e0224a4ac56f96b2931338d4595060ae74c164daffca500e428f56ededc045f'
            '67be2ec0056efee6c8e6cd88f60a2027b50154b0df8b97c0b308064547ac91a3'
            'cea84684259922a3b3c484ec609159513ff2f12b2aa34d2697c6fc1c03bda5ec'
            '9c25c5942c4656845744b83facbab97fda3f18747c8f71c129b928a6bda8d89a'
            '0998de1fd20f531f4f147e42af78137ee44c549dcfc9b6027ca91b5c791af11e'
            '37b86ca3de148a34258e3176dbf41488d9dbd19e93adbd22a062b3c41332ce85'
            '94afbc6a9cb0709f6cd71879bae66454ec26d37c83f49f58e4de28d47678e66b'
            '8dc7285a797c77e917aab1c05847370b71725389b9718c58b4565b40eed80d85'
            'a504f6cf84094e08eaa3cc5b28440261797bf4f06f04993ee46a20628ff2b53c'
            'e096b127a5208f56d368d2cb938933454d7200d70c86b763aa22c38e0ddb8717'
            '8c1c880f2caa9c7ae43281a35410203887ea8eae750fe8d360d0c8bf80fcc6e0'
            '1144d51e5eb980fceeec16004f3645ed04a60fac9e0c7cf88a15c5c1e7a4b89e'
            'dd4b69def2efacf4a6c442202ad5cb93d492c03886d7c61de87696e5a83e2846'
            'c12a25d9b181924f34386e2e678e65d2609ec5e04746731b92ca957f99d66d1f'
            'c8b0cb231659d33c3cfaed4b1f8d7c8305ab170bdd4c77fce85270d7b6a68000'
            '8dbb5ab3cb99e48d97d4e2f2e3df5d0de66f3721b4f7fd94a708089f53245c77'
            'a7aefeacf22c600fafd9e040a985a913643095db7272c296b77a0a651c6a140a'
            'e9f22cbb542591087d2d66dc6dc912b1434330ba3cd13d2df741d869a2c31e89'
            '27471eee564ca3149dd271b0817719b5565a9594dc4d884fe3dc51a5f03832bc'
            '60e295601e4fb33d9bf65f198c54c7eb07c0d1e91e2ad1e0dd6cd6e142cb266d'
            '035ea4b2a7621054f4560471f45336b981538a40172d8f17285910d4e0e0b3ef'
            '006d75ad5e2285aee695367266dd846cfe3019b2e6c493a273c2957b3ad53bab'
            '9f7177679c8d3f8d699ef0566a51349d828436dba04603bc2223f98c60d2d178'
            '1b3929b0b33d1434238b4b512bfea3a85c38a7fbc20dd5e5766433c7c6f3061a'
            '5f6177248c9aff5b2c760432a60989a07bf1b7d546328c507a63ff0600ba84cb')
prepare() {
  cd "${srcdir}/linux-hardened-${_basekernel}-lts"

  # Get the sublevel directly from the linux-hardened source code
  _sub=$(grep -E "^SUBLEVEL = [0-9]+$" < ./Makefile | awk '{print $NF}')

  # disable USER_NS for non-root users by default
  # This is already patched into the kernel!!!!
  #patch -Np1 -i ../0001-add-sysctl-to-disallow-unprivileged-CLONE_NEWUSER-by.patch

  # https://bugzilla.redhat.com/show_bug.cgi?id=1526312
  # https://forum.manjaro.org/t/36269/78
  patch -Np1 -i ../0001-i2c-hid-override-HID-descriptors-for-certain-devices.patch
  patch -Np1 -i ../0002-i2c-hid-properly-terminate-i2c_hid_dmi_desc_override_table_array.patch

  # Add bootsplash - http://lkml.iu.edu/hypermail/linux/kernel/1710.3/01542.html
  patch -Np1 -i "${srcdir}/0001-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0002-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0003-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0004-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0005-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0006-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0007-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0008-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0009-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0010-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0011-bootsplash.patch"
  patch -Np1 -i "${srcdir}/0012-bootsplash.patch"
  # use git-apply to add binary files
  git apply -p1 < "${srcdir}/0013-bootsplash.patch"

  # add aufs4 support
  patch -Np1 -i "${srcdir}/aufs4.19.17+-${_aufs}.patch"
  patch -Np1 -i "${srcdir}/aufs4-base.patch"
  patch -Np1 -i "${srcdir}/aufs4-kbuild.patch"
  patch -Np1 -i "${srcdir}/aufs4-loopback.patch"
  patch -Np1 -i "${srcdir}/aufs4-mmap.patch"
  patch -Np1 -i "${srcdir}/aufs4-standalone.patch"
  patch -Np1 -i "${srcdir}/tmpfs-idr.patch"
  patch -Np1 -i "${srcdir}/vfs-ino.patch"

  # add BFQ scheduler
  patch -Np1 -i "${srcdir}/0001-BFQ-${_bfq}-${_bfqdate}.patch"

  # CPU optimization patch
  msg 'CPU optimization patch'
  patch -Np1 -i "${srcdir}/enable_additional_cpu_optimizations_for_gcc_v8.1%2B_kernel_v4.13%2B.patch"

  # extfat-nofuse module
  cp -rf "${srcdir}/exfat-nofuse-master" './fs/exfat'
  patch -Np1 -i "${srcdir}/exfat.patch"

  # Ck1 patchset
  msg 'Ck1 patched'
  patch -Np1 -i "${srcdir}/ck1.new.patch"

  # Ask the user which subarchitecture they have
  printf "%s" "
        Subarchitectures

  1. AMD Opteron/Athlon64/Hammer/K8 (MK8)
  2. AMD Opteron/Athlon64/Hammer/K8 with SSE3 (MK8SSE3)
  3. AMD 61xx/7x50/PhenomX3/X4/II/K10 (MK10)
  4. AMD Barcelona (MBARCELONA)
  5. AMD Bobcat (MBOBCAT)
  6. AMD Jaguar (MJAGUAR)
  7. AMD Bulldozer (MBULLDOZER)
  8. AMD Piledriver (MPILEDRIVER)
  9. AMD Steamroller (MSTEAMROLLER)
  10. AMD Excavator (MEXCAVATOR)
  11. AMD Zen (MZEN)
  12. Intel P4 / older Netburst based Xeon (MPSC)
  13. Intel Atom (MATOM)
  14. Intel Core 2 (MCORE2)
  15. Intel Nehalem (MNEHALEM)
  16. Intel Westmere (MWESTMERE)
  17. Intel Silvermont (MSILVERMONT)
  18. Intel Sandy Bridge (MSANDYBRIDGE)
  19. Intel Ivy Bridge (MIVYBRIDGE)
  20. Intel Haswell (MHASWELL)
  21. Intel Broadwell (MBROADWELL)
  22. Intel Skylake (MSKYLAKE)
  23. Intel Skylake X (MSKYLAKEX)
  24. Intel Cannon Lake (MCANNONLAKE)
  25. Intel Ice Lake (MICELAKE)
  26. Generic-x86-64 (GENERIC_CPU)
  27. Native optimizations autodetected by GCC (MNATIVE)

  Please choose your cpu's subarch from the above list. If you
  don't know which one to choose or your subarch is unavailable
  then choose 'MNATIVE' or 'GENERIC_CPU. MNATIVE is better but
  the resulting kernel will only run on the computer you compile
  it on. I you would like to build a kernel that will run on
  any X86_64 architecture then choose GENERIC_CPU

  your choice: "

  while true; do
    read SUBARCH < /dev/stdin
    [[ 0 -lt $SUBARCH && $SUBARCH -lt 28 ]] && break
    echo
    echo "Please enter an integer between 1 and 27"
    echo
  done

  # Set the vendor of the chip
  while true;
  do
    case $SUBARCH in
      [1-9]) VENDOR=amd; break
             ;;
      1[0-1]) VENDOR=amd; break
              ;;
      1[2-9]) VENDOR=intel; break
              ;;
      2[0-5]) VENDOR=intel; break
              ;;
      26) VENDOR=generic; break
          ;;
      27) VENDOR=native; break
          ;;
    esac
  done

  # copy proper config to the linux source
  if [ "${CARCH}" = "x86_64" ]; then
    cat "${srcdir}/config.x86_64.${VENDOR}" > ./.config
  else
    cat "${srcdir}/config.${VENDOR}" > ./.config
  fi

  cat "${srcdir}/config.aufs" >> ./.config

  # Array to hold names of subarches
  _subarcharray=(MK8 MK8SSE3 MK10 MBARCELONA MBOBCAT MJAGUAR MBULLDOZER MPILEDRIVER MSTEAMROLLER \
                     MEXCAVATOR MZEN MPSC MATOM MCORE2 MNEHALEM MWESTMERE MSILVERMONT MSANDYBRIDGE \
                     MIVYBRIDGE MHASWELL MBROADWELL MSKYLAKE MSKYLAKEX MCANNONLAKE MICELAKE GENERIC_CPU MNATIVE)

  # Set the subarch in the config file
  sed -i "s|# CONFIG_${_subarcharray[((${SUBARCH}-1))]} is not set|CONFIG_${_subarcharray[((${SUBARCH}-1))]}=y|g" "./.config"

  if [ "${_kernelname}" != "" ]; then
    sed -i "s|CONFIG_LOCALVERSION=.*|CONFIG_LOCALVERSION=\"${_kernelname}\"|g" ./.config
    sed -i "s|CONFIG_LOCALVERSION_AUTO=.*|CONFIG_LOCALVERSION_AUTO=n|" ./.config
  fi

  # set extraversion to pkgrel
  sed -ri "s|^(EXTRAVERSION =).*|\1 -${pkgrel}|" Makefile

  # don't run depmod on 'make install'. We'll do this ourselves in packaging
  sed -i '2iexit 0' scripts/depmod.sh

  # get kernel version
  make prepare

  # load configuration
  # Configure the kernel. Replace the line below with one of your choice.
  #make menuconfig # CLI menu for configuration
  #make nconfig # new CLI menu for configuration
  #make xconfig # X-based configuration
  #make oldconfig # using old config from previous kernel version
  # ... or manually edit .config

  # rewrite configuration
  yes "" | make config >/dev/null
}

build() {
  cd "${srcdir}/linux-hardened-${_basekernel}-lts"

  # build!
  make ${MAKEFLAGS} LOCALVERSION= bzImage modules
}

package_desktop-hardened-lts-419() {
  pkgdesc="The ${pkgbase/linux/Linux} kernel and modules"
  depends=('coreutils' 'linux-firmware' 'kmod' 'mkinitcpio>=0.7')
  optdepends=('crda: to set the correct wireless channels of your country')
  provides=("linux=${pkgver}")
  backup=("etc/mkinitcpio.d/${pkgbase}.preset")
  install=${pkgname}.install

  cd "${srcdir}/linux-hardened-${_basekernel}-lts"

  KARCH=x86

  # get kernel version
  _kernver="$(make LOCALVERSION= kernelrelease)"

  mkdir -p "${pkgdir}"/{boot,usr/lib/modules}
  make LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}/usr" modules_install
  cp arch/$KARCH/boot/bzImage "${pkgdir}/boot/vmlinuz-${_basekernel}${_kernelname}-${CARCH}"

  # add kernel version
  if [ "${CARCH}" = "x86_64" ]; then
	  echo "${pkgver}-${pkgrel}${_kernelname} x64" > "${pkgdir}/boot/${pkgbase}-${CARCH}.kver"
  else
+     echo "${pkgver}-${pkgrel}${_kernelname} x32" > "${pkgdir}/boot/${pkgbase}-${CARCH}.kver"
  fi

  # make room for external modules
  local _extramodules="extramodules-${_basekernel}${_kernelname}"
  ln -s "../${_extramodules}" "${pkgdir}/usr/lib/modules/${_kernver}/extramodules"

  # add real version for building modules and running depmod from hook
  echo "${_kernver}" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_extramodules}/version"

  # remove build and source links
  rm "${pkgdir}"/usr/lib/modules/${_kernver}/{source,build}

  # now we call depmod...
  depmod -b "${pkgdir}/usr" -F System.map "${_kernver}"

  # add vmlinux
  install -Dt "${pkgdir}/usr/lib/modules/${_kernver}/build" -m644 vmlinux

  # sed expression for following substitutions
  local _subst="
    s|%PKGBASE%|${pkgbase}|g
    s|%BASEKERNEL%|${_basekernel}|g
    s|%ARCH%|${CARCH}|g
    s|%KERNVER%|${_kernver}|g
    s|%EXTRAMODULES%|${_extramodules}|g
  "

  # hack to allow specifying an initially nonexisting install file
  sed "${_subst}" "${startdir}/${install}" > "${startdir}/${install}.pkg"
  true && install=${install}.pkg

  # install mkinitcpio preset file
  sed "${_subst}" ${srcdir}/${pkgbase}.preset |
    install -Dm644 /dev/stdin "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"

  # install pacman hooks
  sed "${_subst}" ${srcdir}/60-linux.hook |
    install -Dm644 /dev/stdin "${pkgdir}/usr/share/libalpm/hooks/60-${pkgbase}.hook"
  sed "${_subst}" ${srcdir}/90-linux.hook |
    install -Dm644 /dev/stdin "${pkgdir}/usr/share/libalpm/hooks/90-${pkgbase}.hook"
}

package_desktop-hardened-lts-419-headers() {
  pkgdesc="Header files and scripts for building modules for ${pkgbase/linux/Linux} kernel"
  provides=("linux-headers=$pkgver")

  cd "${srcdir}/linux-hardened-${_basekernel}-lts"
  local _builddir="${pkgdir}/usr/lib/modules/${_kernver}/build"

  install -Dt "${_builddir}" -m644 Makefile .config Module.symvers
  install -Dt "${_builddir}/kernel" -m644 kernel/Makefile

  mkdir "${_builddir}/.tmp_versions"

  cp -t "${_builddir}" -a include scripts

  install -Dt "${_builddir}/arch/${KARCH}" -m644 "arch/${KARCH}/Makefile"
  install -Dt "${_builddir}/arch/${KARCH}/kernel" -m644 "arch/${KARCH}/kernel/asm-offsets.s"

  if [ "${CARCH}" = "i686" ]; then
    install -Dt "${_builddir}/arch/${KARCH}" -m644 "arch/${KARCH}/Makefile_32.cpu"
  fi

  cp -t "${_builddir}/arch/${KARCH}" -a "arch/${KARCH}/include"

  install -Dt "${_builddir}/drivers/md" -m644 drivers/md/*.h
  install -Dt "${_builddir}/net/mac80211" -m644 net/mac80211/*.h

  # http://bugs.archlinux.org/task/13146
  install -Dt "${_builddir}/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # http://bugs.archlinux.org/task/20402
  install -Dt "${_builddir}/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "${_builddir}/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "${_builddir}/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # add xfs and shmem for aufs building
  mkdir -p "${_builddir}"/{fs/xfs,mm}

  # copy in Kconfig files
  find . -name Kconfig\* -exec install -Dm644 {} "${_builddir}/{}" \;

  if [ "${CARCH}" = "x86_64" ]; then
    # add objtool for external module building and enabled VALIDATION_STACK option
    install -Dt "${_builddir}/tools/objtool" tools/objtool/objtool
  fi

  # remove unneeded architectures
  local _arch
  for _arch in "${_builddir}"/arch/*/; do
    [[ ${_arch} == */x86/ ]] && continue
    rm -r "${_arch}"
  done

  # remove files already in linux-docs package
  rm -r "${_builddir}/Documentation"

  # Fix permissions
  chmod -R u=rwX,go=rX "${_builddir}"

  # strip scripts directory
  local _binary _strip
  while read -rd '' _binary; do
    case "$(file -bi "${_binary}")" in
      *application/x-sharedlib*)  _strip="${STRIP_SHARED}"   ;; # Libraries (.so)
      *application/x-archive*)    _strip="${STRIP_STATIC}"   ;; # Libraries (.a)
      *application/x-executable*) _strip="${STRIP_BINARIES}" ;; # Binaries
      *) continue ;;
    esac
    /usr/bin/strip ${_strip} "${_binary}"
  done < <(find "${_builddir}/scripts" -type f -perm -u+w -print0 2>/dev/null)
}
